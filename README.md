# PTML - HTML But in Python

Ever wanted to pull your hair out even more trying to code in html?

THEN PTML IS FOR YOU!

A much more unnecessarily complicated custom-code -to-html wrapper that even supports PYTHONSCRIPT!

Yes, you can write and execute full python programs too!

  

## How do I use this beauty?

Simply create your ptml file and run ``python ptml.py {filename}.ptml``

  

## How does this "language" work?

### Opening classes:
- To open a class, use ``classname:``
For example, ``head:``
- To end a class, use ``end``
```
head:
	h:
        hello world!
	end
end
```

Translates into
```html
<head>
	<h>
		hello world!
	</h>
</head>

```
Note that you don't have to keep using ``end``.
For that reason, there's ``end-class`` and ``end-all``
```
head:
	h:
        hello!
end-all
```
  is the same as
  ```html
  <head>
	  <h>
        hello!
      </h>
</head>
```
and
```
head:
	h:
		b:
        hello!
	end-class
end
```
ends
 ```html
</b>
</h>
```
but keeps ``<head>`` open.
Useful if you have nested classes

You can even have classnames!

`` h class = hello:``
`` <h class=hello>``

## Styling:
- Styling is very similar to normal html.

### Inline Styling:
- Use ``style:``
```
head:
	h class = title:
	style:
	    color: red
		Hello world!
	end
end
```

```html
<head>
	<h  class = title  style="color: red;">
		Hello world!
	</h  class = title>
</head>

```

WARNING: If you want to add a class under a style, add an empty line OR use the escape character ``\:`` Otherwise it won't work!
```
head:
	h class = title:
		style:
		color: red
		b\: <!-- this '\:' -->
			Hello world!
		end
	end
end
```

(Do you see now why this language is unnecessarily complicated?)

### What about my css?

- Just use ``css:``!

```
css:
	.title:
		color: red
	end.
	.body_title:
		color: darkgreen
	end.
end
```

```html

<style>
	.title {
		color: red;
	}
	.body_title {
		color: darkgreen;
	}
</style>

```

  

# Pythonscript:

A brand new word I created. Use python in your html code!

Currently no way to connect the input to what other code there is, but it's like writing a full python program right into ptml!

Note that you have to use print() to get the output in the html file, otherwise you won't get anything

  
  

```

Did you ever want to know what the fib of 200 is?

Using pythonscript, I'll show you!\: <!-- \: doesn't get treated as a label! -->

br:
pythonscript:
def fib(n):
    if n < 2:
        return 1
        result = 0

    while n != 0:
    result += n
    n -= 1
    return result

print(fib(200))
end
end
```

```html
Did you ever want to know what the fib of 200 is?
Using pythonscript, I'll show you!: <!-- see? -->
<br>
20100
</br>
```
