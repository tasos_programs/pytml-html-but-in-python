import sys
import os
from enum import Enum


class Flags(Enum):
    FIRST_STYLE = 0
    STYLE = 1
    FIRST_CSS_STYLE = 2
    CSS_STYLE = 3
    PYTHONSCRIPT = 4


class Ptml:
    def __init__(self, line: str, labels: list, file_name: str, index) -> None:
        self.line = line
        self.labels = labels
        self.flags = {Flags.FIRST_STYLE: False, Flags.STYLE: False,
                      Flags.FIRST_CSS_STYLE: False, Flags.CSS_STYLE: False,
                      Flags.PYTHONSCRIPT: False}
        self.final_result = "<!DOCTYPE html>\n<meta charset=\"utf-8\"/>\n"
        self.file_name = file_name
        self.index = index

    def __is_label(self) -> bool:
        return True if ':' in self.line and not '\:' in self.line else False

    def __proper_spacing(self, optional=0) -> str:
        return '    ' * (len(self.labels) + optional)

    def do_label_actions(self):
        label = self.line[:-1]
        match label:
            case "style":
                self.final_result = self.final_result[:-2] + f" {label}=\""
                self.flags[Flags.STYLE] = True
                self.flags[Flags.FIRST_STYLE] = True
                return
            case "css":
                self.final_result += self.__proper_spacing() + "<style>\n"
                self.labels.append("style")
                self.flags[Flags.CSS_STYLE] = True
                self.flags[Flags.FIRST_CSS_STYLE] = True
                return
            case "pythonscript":
                self.flags[Flags.PYTHONSCRIPT] = True
                return

        # If no matches found
        if self.flags[Flags.STYLE]:
            if self.flags[Flags.FIRST_STYLE]:
                self.flags[Flags.FIRST_STYLE] = False
                self.final_result += f"{'' * len(self.labels)}{self.line};\n"
            else:
                self.final_result += self.__proper_spacing() + \
                    f"{self.line};\n"
            return
        elif self.flags[Flags.CSS_STYLE]:
            if self.flags[Flags.FIRST_CSS_STYLE]:
                self.flags[Flags.FIRST_CSS_STYLE] = False
                self.final_result += f"{self.__proper_spacing()}{self.line[:-1]}" + \
                    ' {\n'
            else:
                self.final_result += f"{'    ' + self.__proper_spacing()}{self.line};\n"
            return

        # ELSE IF NO OTHER MATCHES
        self.final_result += f"{self.__proper_spacing()}<{label}>\n"
        self.labels.append(label)

    def try_do_end_stuff(self) -> bool:
        match self.line:
            case "end":
                # Close the tag and pop the end_labels list
                classname: str = self.labels.pop().split(' ')[0]
                self.final_result += f"{self.__proper_spacing()}</{classname}>\n"
                return True
            case "end.":
                # End for css
                self.final_result += self.__proper_spacing() + '}\n'
                self.flags[Flags.STYLE] = False
                self.flags[Flags.FIRST_CSS_STYLE] = True
                return True
            case "end-silent":
                # end but don't write into output.
                self.labels.pop()
                return True
            case "end-all":
                # End until the labels list is empty
                while self.labels != []:
                    classname: str = self.labels.pop().split(' ')[0]
                    self.final_result += f"{self.__proper_spacing()}</{classname}>\n"
                return True
            case "end-class":
                while len(self.labels) > 1:
                    classname: str = self.labels.pop().split(' ')[0]
                    self.final_result += f"{self.__proper_spacing()}</{classname}>\n"
                return True
        return False

    def end_style(self) -> bool:
        if self.flags[Flags.STYLE] and not ':' in self.line:
            # Close style tag
            self.final_result = self.final_result[:-1] + "\">\n"
            # Add line
            self.final_result += self.__proper_spacing() + self.line + '\n'
            self.flags[Flags.STYLE] = False
            return True

        if self.flags[Flags.STYLE] and '\:' in self.line:
            # Fix for having a class under style: breaking things.
            self.line = self.line.replace("\:", ':')
            # Close style tag
            self.final_result = self.final_result[:-1] + "\">\n"
            # Add line
            self.final_result += self.__proper_spacing() + \
                f"<{self.line[:-1]}>" + '\n'
            self.flags[Flags.STYLE] = False
            self.labels.append(self.line[:-1])
            return True
        return False

    @staticmethod
    def __reset_script_file() -> None:
        # reset script file by opening and closing it
        pythonscript = open("script.py", 'w')
        pythonscript.close()

    def do_pythonscript_stuff(self) -> None:
        if self.line == "end":
            import subprocess
            process = subprocess.Popen(
                "python script.py", shell=True, stdout=subprocess.PIPE)
            self.final_result += self.__proper_spacing() + \
                str(process.stdout.readline())[2:-3] + '\n'
            self.flags[Flags.PYTHONSCRIPT] = False
            self.__reset_script_file()
        else:
            script = open("script.py", 'a')
            unmodified_file = open(self.file_name, 'r').readlines()
            script.write(unmodified_file[self.index] + '\n')
            script.close()

    def interpret_line(self) -> None:
        if self.flags[Flags.PYTHONSCRIPT]:
            return self.do_pythonscript_stuff()
        elif self.__is_label():
            return self.do_label_actions()
        elif self.try_do_end_stuff():
            return
        elif not self.end_style():
            self.line = self.line.replace("\:", ':')
            self.final_result += f"{self.__proper_spacing()}{self.line}\n"


def check_arguments() -> bool:
    if len(sys.argv) < 2:
        print("No file given!", file=sys.stderr)
        return False
    elif not os.path.isfile(sys.argv[1]):
        print("File not found", file=sys.stderr)
        return False
    return True


def get_proper_contents(file_name: str) -> list:
    # For every line, trim and do stuff.
    unmodified_lines = open(file_name).readlines()
    result: list = [line.strip() for line in unmodified_lines]

    return result


def main():
    FILE_NAME: str = sys.argv[1]
    OUTPUT_FILE_NAME: str = FILE_NAME[:-5] + ".html"
    output_file = open(OUTPUT_FILE_NAME, "w")
    proper_code = get_proper_contents(FILE_NAME)

    runner = Ptml(line="", labels=[], file_name=FILE_NAME, index=0)
    for index, line in enumerate(proper_code):
        runner.line = line
        runner.index = index
        runner.interpret_line()
    output_file.write(runner.final_result)


if __name__ == "__main__" and check_arguments():
    main()
